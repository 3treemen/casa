<?php
/*
Template Name: Events
 */

get_header(); ?>
 
 <main id="site-content" role="main">
<?php get_template_part( 'template-parts/featured-image' ); ?>
<?php
	if ( have_posts() ) {

		while ( have_posts() ) {
			the_post();

			get_template_part( 'template-parts/events', get_post_type() );
		}
	}

	?>


</main><!-- #site-content -->

<?php get_template_part( 'template-parts/footer-menus-widgets' ); ?>

<?php get_footer(); ?>