<?php
/**
 * The default template for displaying content
 *
 * Used for both singular and index.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package WordPress
 * @subpackage Twenty_Twenty
 * @since 1.0.0
 */

?>

<article <?php post_class(); ?> id="post-<?php the_ID(); ?>">

	<?php
	// this shows the title and excerpt
	// get_template_part( 'template-parts/entry-header' );

	
	?>
	<div class="post-inner">
		<div class="entry-content">
			<div class="container">
				<?php upcoming_events(); ?>
			</div><!-- /container -->
		</div>
	</div>
	<div class="container-fluid yellow">
		<div class="row black">
			<div class="container">
				<div class="col-12">
					<h2>Terugblik</h2>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="container">
				<div class="entry-content">
					<div class="container">
						<?php past_events(); ?>
					</div>
				</div>
			</div>
		</div>
	</div>			
</article><!-- .post -->
