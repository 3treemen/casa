<?php
/**
 * Displays the featured image
 *
 * @package WordPress
 * @subpackage Twenty_Twenty
 * @since 1.0.0
 */

if ( has_post_thumbnail() && ! post_password_required() ) {

	$featured_img = wp_get_attachment_url( get_post_thumbnail_id($post->ID) ); 
	$featured_img_classes = get_post_meta(get_the_ID(), 'meta-box-classes', true);
	$banner_title = get_post_meta(get_the_ID(), 'meta-banner-title', true);
	$banner_text = get_post_meta(get_the_ID(), 'meta-banner-text', true);
	$banner_align = get_post_meta(get_the_ID(), 'meta-box-valign', true);
?>
<div class="cover-header-wrapper <?php echo $featured_img_classes; ?>">
	<div class="cover-header <?php echo $banner_align; ?>"
		<?php if( $featured_img ) : ?>
		style="background-image: url(<?php echo $featured_img; ?>);"
		<?php endif; ?>
			>
	</div>
	<div class="entry-header">
		<div>
			<?php 
				if($banner_title) {
					echo "<h5>" . $banner_title . "</h5>";
				}
				else {
					the_title('<h5>', '</h5>'); 
				}
				echo "<p>" . $banner_text . "</p>" ?>
		</div>
	</div>

</div>
	<?php
}
else {
	

?>
	<div class="cover-header-wrapper yellow">
		<div class="container">
			<div class="row">
				<div class="col-12">
					<?php the_title('<h1>', '</h1>'); ?>
					<?php the_excerpt(); ?> 
				</div>
			</div>
		</div>
	</div>
<?php
}