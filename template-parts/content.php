<?php
/**
 * The default template for displaying content
 *
 * Used for both singular and index.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package WordPress
 * @subpackage Twenty_Twenty
 * @since 1.0.0
 */

?>

<article <?php post_class(); ?> id="post-<?php the_ID(); ?>">

	<?php
	// this shows the title and excerpt
	get_template_part( 'template-parts/entry-header' );

	// if ( ! is_search() ) {
	// 	get_template_part( 'template-parts/featured-image' );
	// }

	?>
	<div class="post-inner <?php echo is_page_template( 'templates/template-full-width.php' ) ? '' : 'thin'; ?> ">
		<div class="entry-content">
			<?php
			if ( is_search() || ! is_singular() && 'summary' === get_theme_mod( 'blog_content', 'full' ) ) {
				the_excerpt();
			} else {
				the_content( __( 'Continue reading', 'twentytwenty' ) );
			}

			?>
	</div><!-- .entry-content -->
	</div><!-- .post-inner -->
	
	<?php 
		if(get_post_type( get_the_id()) == 'post') {
			more_news();

		}
?>
		
<?php
		if(get_post_type( get_the_ID()) == 'event' ){
			$event_date = get_post_meta(get_the_ID(), 'meta-box-extra-date', true);
			$event_start = get_post_meta(get_the_ID(), 'meta-box-event-start', true);
			$event_end_date = get_post_meta(get_the_ID(), 'meta-box-end-date', true);
			$event_location = get_post_meta(get_the_ID(), 'meta-box-event-location', true);
			$event_price = get_post_meta(get_the_ID(), 'meta-box-event-price', true);
			$event_fb = get_post_meta(get_the_ID(), 'meta-box-event-fb', true);
			$now = date_i18n('l j F');
			$date = date_i18n('l j F', strtotime($event_date));	
			$enddate = date_i18n('l j F', strtotime($event_end_date));
			echo "<div class='container'>";
			echo "<dl class='row event-meta'>";
			echo "<dt class='col-md-2 col-12'>Datum</dt><dd class='col-md-9 col-12'>";
			echo $date; if($event_end_date) { echo " - " .$enddate;}
			echo "</dd>";
			if($event_start) {
				echo "<dt class='col-md-2 col-12'>Aanvang</dt><dd class='col-md-9 col-12'>";
				echo $event_start;
				echo "</dd>";
			}
			if($event_location) {
				echo "<dt class='col-md-2 col-12'>Locatie</dt><dd class='col-md-9 col-12'>";
				echo $event_location;
				echo "</dd>";
			}
			if($event_price) {
				echo "<dt class='col-md-2 col-12'>Entree</dt><dd class='col-md-9 col-12'>";
				echo $event_price;
				echo "</dd>";
			}
			if($event_fb) {
				echo "<dt class='col-md-2 col-12'>Social media</td><dd class='col-md-9 col-12'>";
				echo "<a href='".$event_fb . "'>Bekijk op Facebook</a>";
				echo "</dd>";
			}
			echo "</dl>";
			echo "</div>";
		}
	?>
	

</article><!-- .post -->
