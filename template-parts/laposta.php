<!-- Begin Mailchimp Signup Form -->
<!-- <link href="//cdn-images.mailchimp.com/embedcode/horizontal-slim-10_7.css" rel="stylesheet" type="text/css"> -->

<div class="section-inner ">

    <div class="row">
        <div class="col-12 col-md-5">
            <div class="header">Blijf op de hoogte! Schrijf je in voor onze nieuwsbrief</div>
        </div>
        <div class="col-12 col-md-6">
        <form method="post" action="https://stichting-casa.email-provider.nl/subscribe/post/index.php" accept-charset="utf-8">
            <input type="hidden" name="next" value="https://casa-arnhem.nl/bedankt" />
            <input type="hidden" name="a" value="vk0epabvve" />
            <input type="hidden" name="l" value="yhjuibrvr9" />
            <label class="hidden" for="id-CwSbKs7FCq">e-mail adres</label>
            <input type="text" name="CwSbKs7FCq" class="email" placeholder="e-mailadres" arial-label="e-mail adres" required id="id-CwSbKs7FCq">
            <label class="hidden" for="id-iLkjPhqEk0">Naam</label>
            <input type="text" name="iLkjPhqEk0" class="email" placeholder="naam" aria-label="naam" id="id-iLkjPhqEk0">   
            <label class="hidden" for="email">email voor wachtwoord reset</label>         
            <input autocomplete="new-password" type="email" id="email" name="email" placeholder="Your e-mail here" style="position:absolute;top:-9999px;left:-9999px;">
            <div class="clear">
                <input type="submit" value="Aanmelden" class="button" />
            </div>
        </form>


        </div>        
    </div>
    <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
    <div style="position: absolute; left: -5000px;" aria-hidden="true">
        <input type="text" name="b_981093ceaf72f2f48343edf05_28d16a7caa" tabindex="-1" value="">
    </div>
</div>

<!--End mc_embed_signup-->
