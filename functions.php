<?php

// include events post type
require_once('post-types/event.php');

add_action( 'after_setup_theme', 'wpdocs_theme_setup' );
function wpdocs_theme_setup() {
    add_image_size( 'list-thumb', 500 ); // 300 pixels wide (and unlimited height)
}
/* enqueue scripts and style from parent theme */        
function twentytwenty_styles() {
	wp_enqueue_style( 'parent', get_template_directory_uri() . '/style.css' );
    wp_enqueue_script( 'casa', get_stylesheet_directory_uri() . '/js/casa.js', array(), '1.0.0', true );

}
add_action( 'wp_enqueue_scripts', 'twentytwenty_styles');


/**
 * Loads the image management javascript
 */
function casa_image_enqueue() {
    // global $typenow;
	wp_enqueue_media();

	// Registers and enqueues the required javascript.
	wp_register_script( 'meta-box-image', get_stylesheet_directory_uri() . '/js/meta-box-image.js', array( 'jquery' ) );
	wp_localize_script( 'meta-box-image', 'meta_image',
		array(
			'title' => __( 'Choose or Upload an Image', 'casa-arnhem' ),
			'button' => __( 'Use this image', 'casa-arnhem' ),
		)
	);
	wp_enqueue_script( 'meta-box-image' );

}
add_action( 'admin_enqueue_scripts', 'casa_image_enqueue' );


// disable big image resize
add_filter( 'big_image_size_threshold', '__return_false' );


// enable excepts on page
add_post_type_support( 'page', 'excerpt' );

// add another image size
add_image_size( 'small', 450, 300 );
add_image_size('midsized', 600, 600);

// Unregister some of the TwentyTwenty sidebars
 function remove_some_widgets(){
    unregister_sidebar( 'sidebar-1' );
    unregister_sidebar( 'sidebar-2' );
}
add_action( 'widgets_init', 'remove_some_widgets', 11 );

// limit excerpt lenth to 20
function custom_excerpt_length( $length ) {
	return 20;
}
add_filter( 'excerpt_length', 'custom_excerpt_length', 999 );


// Register my custom sidebars in the footer
function sidebar_one() {
    $args = array(
        'id' => 'sidebar-one',
        'name' => __( 'Footer #1', 'text_domain'),
        'description' => __( 'Footer 1 widget.', 'text_domain'),
        'before_title' => '<h3 class="widget_title">',
        'after_title' => '</h3>',
        'before_widget' => '<section id="%1$s" class="widget %2$s">',
        'after_widget' => '</section>',
    );
    register_sidebar( $args );
}

function sidebar_two() {
    $args = array(
        'id' => 'sidebar-two',
        'name' => __( 'Footer #2', 'text_domain'),
        'description' => __( 'Footer 2 widget.', 'text_domain'),
        'before_title' => '<h3 class="widget_title">',
        'after_title' => '</h3>',
        'before_widget' => '<section id="%1$s" class="widget %2$s">',
        'after_widget' => '</section>',
    );
    register_sidebar( $args );
}

function sidebar_three() {
    $args = array(
        'id' => 'sidebar-three',
        'name' => __( 'Footer #3', 'text_domain'),
        'description' => __( 'Footer 3 widget.', 'text_domain'),
        'before_title' => '<h3 class="widget_title">',
        'after_title' => '</h3>',
        'before_widget' => '<section id="%1$s" class="widget %2$s">',
        'after_widget' => '</section>',
    );
    register_sidebar( $args );
}

function sidebar_four() {
    $args = array(
        'id' => 'sidebar-four',
        'name' => __( 'Footer #4', 'text_domain'),
        'description' => __( 'Footer 4 widget.', 'text_domain'),
        'before_title' => '<h3 class="widget_title">',
        'after_title' => '</h3>',
        'before_widget' => '<section id="%1$s" class="widget %2$s">',
        'after_widget' => '</section>',
    );
    register_sidebar( $args );
}
add_action ('widgets_init', 'sidebar_one' );
add_action ('widgets_init', 'sidebar_two' );
add_action ('widgets_init', 'sidebar_three' );
add_action ('widgets_init', 'sidebar_four' );

// enable categories on pages
function add_categories_to_pages() {
	register_taxonomy_for_object_type( 'category', 'page' );
}
add_action( 'init', 'add_categories_to_pages' );

// enable tags on pages
function add_tags_to_pages() {
	register_taxonomy_for_object_type( 'post_tag', 'page' );
}
add_action( 'init', 'add_tags_to_pages');

// show listing of posts on post-page
function more_news() {
	$args = array( 
		'post_type' => 'post',
		'post_status' => 'publish',
		'order_by' => 'date'
		);	
	// The Query
	$the_query = new WP_Query( $args );
	
	// The Loop
	if ( $the_query->have_posts() && $the_query->post_count > 1) {
	?>
		<div class="container-fluid">
			<div class="row black">
				<div class="container">
					<div class="col-12">
						<h2>Ander nieuws</h2>
					</div>
				</div>
			</div>
		</div>
		<div class="container">
			<div class="entry-content">
		<?php
		$cur_id = get_the_ID();
		while ( $the_query->have_posts() ) {
			$the_query->the_post();
			$excerpt = get_the_excerpt();
			$permalink = get_permalink();
			$categories = get_the_category();
			$post_id = get_the_id();
			$m_meta_datum = get_post_meta(get_the_ID(), 'meta-box-extra-date', true);
			$m_meta_datum = date_i18n( 'l j F' , strtotime($m_meta_datum));
			$thumb_url = get_the_post_thumbnail_url($post = null, $size = 'medium');
			$terugblik = get_post_meta(get_the_ID(), 'terugblik', true);
			if($post_id !== $cur_id ) {
		
				echo "<div class='row alignfull'>";
			
				echo "<div class='col-12 col-sm-4'>";
				if($thumb_url) {
					echo "<a href='". $permalink ."'>";	
						echo "<img src='" . $thumb_url . "' alt='Featured Image' class='responsive' />";
					echo "</a>";
				}
				echo "</div>"; // col
				echo "<div class='col-12 col-sm-8'>";
					echo "<a href='". $permalink ."'>";
					echo "<div class='date summary'>" . $m_meta_datum . "</div>";
					the_title('<h5>', '</h5>');						
					if ( ! empty( $categories ) ) {
						echo "<div class='cat'>";
						foreach( $categories as $cat ) {
							if( $cat->name !== 'Uncategorized' ) {
								echo esc_html( $cat->name ) . " " ;
							}
						}
						echo "</div>";   
					}
					echo "</a>";
				echo "</div>";// col
			echo "</div>";
			}
			
		}
		?>
		</div>
	</div>
	<?php
	
	} else {
		// no posts found
	}
	/* Restore original Post Data */
	wp_reset_postdata();

}

// show posts on homepage
function newsitems_home() {
	$args = array( 
		'post_type' => 'post',
		'post_status' => 'publish',
		'order_by' => 'date'
		);
		
	// The Query
	$the_query = new WP_Query( $args );
	
	// The Loop
	if ( $the_query->have_posts() ) {
	
		while ( $the_query->have_posts() ) {
			$the_query->the_post();
			$publish_date = get_the_date();
			$permalink = esc_url( get_permalink());
			$date = date_i18n('l j F', strtotime($publish_date));
			$excerpt = get_the_excerpt(); 
			$excerpt = substr($excerpt, 0, 160);
			$categories = get_the_category();
			$thumb_url = get_the_post_thumbnail_url($post = null, $size = 'list-thumb');
			$home_image = get_post_meta(get_the_ID(), 'meta-image', true);
			
			echo "<div class='col col-12 col-sm-6 col-lg-4'>";
			echo "<a href=" . $permalink . ">"; 	
			echo "<span class='tag'>Nieuws</span>";
			if($home_image) {
				echo  "<img src='" . $home_image ." ' alt='' class='responsive' />";
			} else {
				if($thumb_url) {
						echo "<img src='" . $thumb_url . "' alt='Featured Image' class='responsive' />";
					}
			}
			// the_post_thumbnail( 'medium_large');
			echo "<div class='date'>" .$publish_date . "</div>";
			echo "<h4>" . get_the_title() . "</h4>";
			if( has_excerpt() ) {
				echo "<div class='description'>" . $excerpt . " [...]</div>";
	
			}
			if ( ! empty( $categories )) {
				echo "<div class='cat'>";
				foreach( $categories as $cat ){
					if( $cat->name !== 'Uncategorized' ) {
						echo esc_html( $cat->name ) . "  " ;   
					}
				}
				echo "</div>";
			}
			echo "</a>";
			echo "</div>";
		}
	
	} else {
		// no posts found
	}
	/* Restore original Post Data */
	wp_reset_postdata();
}


// *************************************************
// create custom fields for banner
// *************************************************


function add_banner_meta_box() {
    add_meta_box(
        'banner_meta_box', // $id
        'Banner', // $title
        'banner_meta_box_markup', // $callback
        array('event','page','post'), // $screen
        'side', // $context
        'high', // $priority
        null
    );
}
add_action( 'add_meta_boxes', 'add_banner_meta_box' );

// create the input fields
function banner_meta_box_markup($object) {
	$meta = get_post_meta( $object->ID );
	wp_nonce_field(basename(__FILE__), "meta-box-nonce");
	?>
	<div>
		<label for="meta-box-classes">
			Banner textbox (right, small)
			<input type="text" name="meta-box-classes" value="<?php echo get_post_meta($object->ID, "meta-box-classes", true); ?>">
		</label>
	</div>
	<div>
		<label for="meta-banner-title">Banner title
			<input name="meta-banner-title" type="text" value="<?php echo get_post_meta($object->ID, "meta-banner-title", true); ?>">
		</label>
	</div>
	<div>
		<label for="meta-banner-text">Banner text
			<input name="meta-banner-text" type="text" value="<?php echo get_post_meta($object->ID, "meta-banner-text", true); ?>">
		</label>
	</div>
	<div>
		<label for="meta-box-valign">Vertical align<br>
		<select name="meta-box-valign">
			<?php 
				$option_values = array("top", "center", "bottom");

				foreach($option_values as $key => $value) 
				{
					if($value == get_post_meta($object->ID, "meta-box-valign", true))
					{
						?>
							<option selected><?php echo $value; ?></option>
						<?php    
					}
					else
					{
						?>
							<option><?php echo $value; ?></option>
						<?php
					}
				}
			?>
		</select>
		</label>
	</div>
	<div>
    	<label for="meta-image" class="prfx-row-title"><?php _e( 'Homepage image', 'casa-arnhem' )?></label>
		<input type="text" class="hidden" name="meta-image" id="meta-image" value="<?php if ( isset ( $meta['meta-image'] ) ) echo $meta['meta-image'][0]; ?>" />
		<?php if(isset($meta['meta-image'])) echo '<img src="' . $meta['meta-image'][0] . '" />' ?>
    	<input type="button" id="meta-image-button" class="button" value="<?php _e( 'Choose or Upload an Image', 'casa-arnhem' )?>" />
	</div>
<?php
}

// save the fields on update page
function save_banner_meta_box($post_id, $post, $update) {
	if (!isset($_POST["meta-box-nonce"]) || !wp_verify_nonce($_POST["meta-box-nonce"], basename(__FILE__)))
	return $post_id;

	if(!current_user_can("edit_post", $post_id))
		return $post_id;

	if(defined("DOING_AUTOSAVE") && DOING_AUTOSAVE)
		return $post_id;
	
	$meta_box_classes_value = "";
	$meta_banner_title_value = "";
	$meta_banner_text_value = "";
	$meta_box_valign_value = "";
	$meta_image_value = "";


	if(isset($_POST["meta-box-classes"])) {
		$meta_box_classes_value = $_POST["meta-box-classes"];
	}
	update_post_meta($post_id, "meta-box-classes", $meta_box_classes_value);

	if(isset($_POST["meta-banner-title"])) {
		$meta_banner_title_value = $_POST["meta-banner-title"];
	}
	update_post_meta($post_id, "meta-banner-title", $meta_banner_title_value);

	if(isset($_POST["meta-banner-text"])) {
		$meta_banner_text_value = $_POST["meta-banner-text"];
	}
	update_post_meta($post_id, "meta-banner-text", $meta_banner_text_value);

	if(isset($_POST["meta-box-valign"])){
        $meta_box_valign_value = $_POST["meta-box-valign"];
    }   
    update_post_meta($post_id, "meta-box-valign", $meta_box_valign_value);
	
	if( isset($_POST["meta-image"])){
		$meta_image_value = $_POST["meta-image"];
	}
	update_post_meta($post_id, "meta-image", $meta_image_value );

}
add_action("save_post", "save_banner_meta_box", 10, 4);


// create a page summary
function page_summary() {
	global $post;
	$args = array(
		'post_type'      => 'page',
		'post_parent'    => $post->ID,
		'order'          => 'ASC',
		'orderby'        => 'menu_order'
	);


	$parent = new WP_Query( $args );

	if ( $parent->have_posts() ) : 

		while ( $parent->have_posts() ) : $parent->the_post(); 
			$excerpt = get_the_excerpt();
			$post_id = get_the_id();
			$thumbnail = get_the_post_thumbnail($post_id);
			$permalink = get_permalink();

			echo "<div class='row'>";
			echo "<a href='" . $permalink . "'>";
			echo "<div class='item'>";
			if($thumbnail) {
				echo get_the_post_thumbnail( $post_id, 'medium', array( 'class' => 'alignleft') );
			}
			the_title('<h5>' , '</h5>' );
			if ( has_excerpt() ) {
				echo "<span class='description'>" . $excerpt . "</span>";
			}
			echo "</div>";
			echo "</a>";
			echo "</div>";
			
		?>
	<?php
		 endwhile; 

	endif; 
	wp_reset_postdata(); 
}

// show template used, for debug purpose
function meks_which_template_is_loaded() {
	if ( is_super_admin() ) {
		global $template;
		print_r( $template );
	}
}
add_action( 'wp_footer', 'meks_which_template_is_loaded' );

// flush_rewrite_rules();
add_action('wp_dashboard_setup', 'my_custom_dashboard_widgets'); 
function my_custom_dashboard_widgets() {
	global $wp_meta_boxes;
 	wp_add_dashboard_widget('custom_help_widget', 'Documentatie', 'custom_dashboard_help');
}
 
function custom_dashboard_help() {
	include 'documentation.txt';
}

// ****************************************************
// event meta boxes 
//*****************************************************

// create the field wrapper
function add_event_meta_box() {
    add_meta_box(
        'event_meta_box', // $id
        'Event', // $title
        'event_meta_box_markup', // $callback
        'event', // $screen
        'side', // $context
        'high', // $priority
        null
    );
}
add_action( 'add_meta_boxes', 'add_event_meta_box' );


// create the input fields
function event_meta_box_markup($object) {
	$meta = get_post_meta( $object->ID );
	$show_on_homepage = ( isset( $meta['show-on-homepage'][0] ) &&  '1' === $meta['show-on-homepage'][0] ) ? 1 : 0;
	$terugblik = ( isset( $meta['terugblik'][0] ) &&  '1' === $meta['terugblik'][0] ) ? 1 : 0;

	wp_nonce_field(basename(__FILE__), "meta-box-nonce");

    ?>
        <div>
            <label for="meta-box-extra-date">Event Start Date<br>
            	<input name="meta-box-extra-date" type="date" value="<?php echo get_post_meta($object->ID, "meta-box-extra-date", true); ?>">
			</label>
		</div>
	
		<div>
			<label for="meta-box-event-start">Event Start Time
				<input name="meta-box-event-start" type="text" value="<?php echo get_post_meta($object->ID, "meta-box-event-start", true); ?>">
			</label>
		</div>
        <div>
            <label for="meta-box-end-date">Event End Date<br>
            	<input name="meta-box-end-date" type="date" value="<?php echo get_post_meta($object->ID, "meta-box-end-date", true); ?>">
			</label>
		</div>

		<div>
			<label for="meta-box-event-location">Event Location
				<input name="meta-box-event-location" type="text" value="<?php echo get_post_meta($object->ID, "meta-box-event-location", true); ?>">
			</label>
		</div>
		<div>
			<label for="meta-box-event-price">Event Price
				<input name="meta-box-event-price" type="text" value="<?php echo get_post_meta($object->ID, "meta-box-event-price", true); ?>">
			</label>
		</div>
		<div>
			<label for="meta-box-event-fb">Event Facebook
				<input name="meta-box-event-fb" type="text" value="<?php echo get_post_meta($object->ID, "meta-box-event-fb", true); ?>">
			</label>
		</div>
		<div>
			<label>
				Show on Homepage<br>
				<input type="checkbox" name="show-on-homepage" value="1" <?php checked( $show_on_homepage, 1 ); ?> />
			</label>	
		</div>
		<div>
			<label>
				Terugblik<br>
				<input type="checkbox" name="terugblik" value="1" <?php checked( $terugblik, 1 ); ?> />
			</label>	
		</div>
    <?php  
}

// save the fields on update event
function save_event_meta_box($post_id, $post, $update) {
    if (!isset($_POST["meta-box-nonce"]) || !wp_verify_nonce($_POST["meta-box-nonce"], basename(__FILE__)))
        return $post_id;

    if(!current_user_can("edit_post", $post_id))
        return $post_id;

    if(defined("DOING_AUTOSAVE") && DOING_AUTOSAVE)
        return $post_id;

    $slug = "event";
    if($slug != $post->post_type)
        return $post_id;

	$meta_box_extra_date_value = "";
	$meta_box_end_date_value = "";
	$meta_box_event_start_value = "";
	$meta_box_event_location_value = "";
	$meta_box_event_price_value = "";
	$meta_box_event_fb_value = "";

    if(isset($_POST["meta-box-extra-date"])) {
        $meta_box_extra_date_value = $_POST["meta-box-extra-date"];
    }   
    update_post_meta($post_id, "meta-box-extra-date", $meta_box_extra_date_value);

    if(isset($_POST["meta-box-event-start"])) {
        $meta_box_event_start_value = $_POST["meta-box-event-start"];
    }   
    update_post_meta($post_id, "meta-box-event-start", $meta_box_event_start_value);

    if(isset($_POST["meta-box-end-date"])) {
        $meta_box_end_date_value = $_POST["meta-box-end-date"];
    }   
    update_post_meta($post_id, "meta-box-end-date", $meta_box_end_date_value);

	if(isset($_POST["meta-box-event-location"])) {
        $meta_box_event_location_value = $_POST["meta-box-event-location"];
    }   
    update_post_meta($post_id, "meta-box-event-location", $meta_box_event_location_value);

	if(isset($_POST["meta-box-event-price"])) {
        $meta_box_event_price_value = $_POST["meta-box-event-price"];
    }   
    update_post_meta($post_id, "meta-box-event-price", $meta_box_event_price_value);

	if(isset($_POST["meta-box-event-fb"])) {
        $meta_box_event_fb_value = $_POST["meta-box-event-fb"];
    }   
    update_post_meta($post_id, "meta-box-event-fb", $meta_box_event_fb_value);

	$show_on_homepage = ( isset( $_POST['show-on-homepage'] ) && '1' === $_POST['show-on-homepage'] ) ? 1 : 0;
	update_post_meta( $post_id, 'show-on-homepage', esc_attr( $show_on_homepage ) );

	$terugblik = ( isset( $_POST['terugblik'] ) && '1' === $_POST['terugblik'] ) ? 1 : 0;
	update_post_meta( $post_id, 'terugblik', esc_attr( $terugblik ) );



}
add_action("save_post", "save_event_meta_box", 10, 3);


function show_categories() {
	$categories = get_the_category();
	if ( ! empty( $categories )) {
		foreach( $categories as $cat ){
			if( $cat->name !== 'Uncategorized' ) {
				echo esc_html( $cat->name ) . "  " ;   
			}
		}
	}
}


// Debug meta-data
// add_action('wp_head', 'output_all_postmeta' );
function output_all_postmeta() {

	$postmetas = get_post_meta(get_the_ID());

	foreach($postmetas as $meta_key=>$meta_value) {
		echo $meta_key . ' : ' . $meta_value[0] . '<br/>';
	}
}
