<?php

/**
 * Registers the `event` post type.
 */
function event_init() {
	register_post_type( 'event', array(
		'labels'                => array(
			'name'                  => __( 'Events', 'casa' ),
			'singular_name'         => __( 'Event', 'casa' ),
			'all_items'             => __( 'All Events', 'casa' ),
			'archives'              => __( 'Event Archives', 'casa' ),
			'attributes'            => __( 'Event Attributes', 'casa' ),
			'insert_into_item'      => __( 'Insert into event', 'casa' ),
			'uploaded_to_this_item' => __( 'Uploaded to this event', 'casa' ),
			'featured_image'        => _x( 'Cover Image', 'event', 'casa' ),
			'set_featured_image'    => _x( 'Set Cover image', 'event', 'casa' ),
			'remove_featured_image' => _x( 'Remove Cover image', 'event', 'casa' ),
			'use_featured_image'    => _x( 'Use as Cover image', 'event', 'casa' ),
			'filter_items_list'     => __( 'Filter events list', 'casa' ),
			'items_list_navigation' => __( 'Events list navigation', 'casa' ),
			'items_list'            => __( 'Events list', 'casa' ),
			'new_item'              => __( 'New Event', 'casa' ),
			'add_new'               => __( 'Add New', 'casa' ),
			'add_new_item'          => __( 'Add New Event', 'casa' ),
			'edit_item'             => __( 'Edit Event', 'casa' ),
			'view_item'             => __( 'View Event', 'casa' ),
			'view_items'            => __( 'View Events', 'casa' ),
			'search_items'          => __( 'Search events', 'casa' ),
			'not_found'             => __( 'No events found', 'casa' ),
			'not_found_in_trash'    => __( 'No events found in trash', 'casa' ),
			'parent_item_colon'     => __( 'Parent Event:', 'casa' ),
			'menu_name'             => __( 'Events', 'casa' ),
		),
		'public'                => true,
		'hierarchical'          => false,
		'show_ui'               => true,
		'show_in_nav_menus'     => true,
		'supports'              => array( 'title', 'editor' , 'thumbnail', 'excerpt', 'page-attributes', 'custom-fields'),
		'has_archive'           => true,
		'rewrite'               => true,
		'query_var'             => true,
		'menu_position'         => null,
		'menu_icon'             => 'dashicons-admin-post',
		'show_in_rest'          => true,
		'rest_base'             => 'event',
		'rest_controller_class' => 'WP_REST_Posts_Controller',
	) );

}
add_action( 'init', 'event_init' );

/**
 * Sets the post updated messages for the `event` post type.
 *
 * @param  array $messages Post updated messages.
 * @return array Messages for the `event` post type.
 */
function event_updated_messages( $messages ) {
	global $post;

	$permalink = get_permalink( $post );

	$messages['event'] = array(
		0  => '', // Unused. Messages start at index 1.
		/* translators: %s: post permalink */
		1  => sprintf( __( 'Event updated. <a target="_blank" href="%s">View event</a>', 'casa' ), esc_url( $permalink ) ),
		2  => __( 'Custom field updated.', 'casa' ),
		3  => __( 'Custom field deleted.', 'casa' ),
		4  => __( 'Event updated.', 'casa' ),
		/* translators: %s: date and time of the revision */
		5  => isset( $_GET['revision'] ) ? sprintf( __( 'Event restored to revision from %s', 'casa' ), wp_post_revision_title( (int) $_GET['revision'], false ) ) : false,
		/* translators: %s: post permalink */
		6  => sprintf( __( 'Event published. <a href="%s">View event</a>', 'casa' ), esc_url( $permalink ) ),
		7  => __( 'Event saved.', 'casa' ),
		/* translators: %s: post permalink */
		8  => sprintf( __( 'Event submitted. <a target="_blank" href="%s">Preview event</a>', 'casa' ), esc_url( add_query_arg( 'preview', 'true', $permalink ) ) ),
		/* translators: 1: Publish box date format, see https://secure.php.net/date 2: Post permalink */
		9  => sprintf( __( 'Event scheduled for: <strong>%1$s</strong>. <a target="_blank" href="%2$s">Preview event</a>', 'casa' ),
		date_i18n( __( 'M j, Y @ G:i', 'casa' ), strtotime( $post->post_date ) ), esc_url( $permalink ) ),
		/* translators: %s: post permalink */
		10 => sprintf( __( 'Event draft updated. <a target="_blank" href="%s">Preview event</a>', 'casa' ), esc_url( add_query_arg( 'preview', 'true', $permalink ) ) ),
	);

	return $messages;
}
add_filter( 'post_updated_messages', 'event_updated_messages' );


// enable categories on pages
function add_categories_to_events() {
	register_taxonomy_for_object_type( 'category', 'event' );
}
add_action( 'init', 'add_categories_to_events' );

// enable tags on pages
function add_tags_to_events() {
	register_taxonomy_for_object_type( 'post_tag', 'event' );
}
add_action( 'init', 'add_tags_to_events');



// show in the admincolumn if an event is set for homepage
function add_homepage_column($page_columns) {
	$page_columns['show_on_homepage'] = 'Homepage';
	return $page_columns;
}

function display_homepage_column( $column_name, $id ) {
	if ( $column_name === 'show_on_homepage') {
		$hp = get_post_meta( get_the_ID(), 'show-on-homepage', true );
		if ($hp == 1) {
			echo 'Homepage';
		}	
	}
}
add_filter( 'manage_event_posts_columns', 'add_homepage_column' );
add_action( 'manage_event_posts_custom_column', 'display_homepage_column', 5, 2);


// show in the admincolumn if an event is set for terugblik
function add_terugblik_column($page_columns) {
	$page_columns['terugblik'] = 'Terublik';
	return $page_columns;
}

function display_terugblik_column( $column_name, $id ) {
	if ( $column_name === 'terugblik') {
		$hp = get_post_meta( get_the_ID(), 'terugblik', true );
		if ($hp == 1) {
			echo 'Terugblik';
		}	
	}
}
add_filter( 'manage_event_posts_columns', 'add_terugblik_column' );
add_action( 'manage_event_posts_custom_column', 'display_terugblik_column', 5, 2);



// show in the admincolumn if a page is set for homepage
function add_event_order_column($event_order_columns) {
	$event_order_columns['menu_order'] = 'Order';
	return $event_order_columns;
}
add_filter( 'manage_event_posts_columns', 'add_event_order_column' );

function display_event_order_column( $name ) {
	global $post;

	switch($name) {
		case 'menu_order':
			$order = $post->menu_order;
			echo $order;
			break;
		default:
			break;
	}
}
add_action( 'manage_event_posts_custom_column', 'display_event_order_column', 5, 2);


// ***************************************************
// lets create items on the homepage for events that are
// marked to show on homepage 
// ***************************************************

// actuele events
function homepage_events ( $terugblik) {
	$args = array(
		// Arguments for your query.
		'post_type' => 'event',
		'post_status' => 'publish',
		'meta_query' => array(
			array(
				'key' => 'show-on-homepage',
				'value' => 1,
			),
			array(
				'key' => 'terugblik',
				'value' => $terugblik,
			)
			),
		'orderby' => 'menu_order',
		'order' => 'ASC'
		
	);
	 
	// Custom query.
	$query = new WP_Query( $args );
	$count = 0;
	// Check that we have query results.
	if ( $query->have_posts() ) {
		// Start looping over the query results.
		while ( $query->have_posts() ) {
			$count++;
			$query->the_post();
			$m_meta_datum = get_post_meta(get_the_ID(), 'meta-box-extra-date', true);
			$m_meta_end_datum = get_post_meta(get_the_ID(), 'meta-box-end-date', true);
			// for comparising
			$curdate = date_i18n('Ymd');
			$postdate = date_i18n('Ymd', strtotime($m_meta_datum));
			$m_meta_datum = date_i18n('l j F', strtotime($m_meta_datum));
			$enddate = date_i18n('l j F', strtotime($m_meta_end_datum));
			$categories = get_the_category();
			$tags = get_the_tags();
			$excerpt = substr(get_the_excerpt(), 0, 160);
			$thumb_url = get_the_post_thumbnail_url($post = null, $size = 'list-thumb');
			$home_image = get_post_meta(get_the_ID(), 'meta-image', true);
			$permalink = get_permalink();
			
			echo "<div class='col col-12 col-sm-6 col-lg-4'>";
		
			echo "<a href=". $permalink . ">";
			if($terugblik == 0 ) {
				echo "<span class='tag'>Programma</span>";
			} else {
				echo "<span class='tag'>Terugblik</span>";
			}
			if($home_image) {
				echo  "<img src='" . $home_image ." ' alt='' class='responsive' />";
			} else {
				if($thumb_url) {
						echo "<img src='" . $thumb_url . "' alt='Featured Image' class='responsive' />";
					}
			}
			if($m_meta_end_datum) {
				echo "<div class='date'>" .$m_meta_datum . " - " .$enddate . "</div>";

			}else {
				echo "<div class='date'>" .$m_meta_datum . "</div>";

			}
				echo the_title('<h4>', '</h4>');
				if( has_excerpt() ) {
					echo "<div class='description'>" . $excerpt . " [...]</div>";

				}
				if ( ! empty( $categories ) ) {
					echo "<div class='cat'>" . esc_html( $categories[0]->name ) . "</div>";   
				}
				echo "</a>";
			echo "</div>";
		}
	}
	 
	// Restore original post data.
	wp_reset_postdata();
}

function upcoming_events() {
	$args = array(
		'post_type' => 'event',
		'orderby' => 'menu_order',
		'meta_key'   => 'terugblik',
		'order' => 'ASC',
		'meta_query' => array(
			array(
				'key' => 'terugblik',
				'value' => 1,
				'compare' => '!=' 
			)	
		)
	);

	// Custom query.
	$query = new WP_Query( $args );

	// Check that we have query results.
	if ( $query->have_posts() ) {

		// Start looping over the query results.
		while ( $query->have_posts() ) {

			$query->the_post();
			$excerpt = get_the_excerpt();
			$permalink = get_permalink();
			$categories = get_the_category();
			$post_id = get_the_id();
			$m_meta_datum = get_post_meta(get_the_ID(), 'meta-box-extra-date', true);
			$m_meta_datum = date_i18n( 'l j F' , strtotime($m_meta_datum));
			$enddate = get_post_meta(get_the_ID(), 'meta-box-end-date', true);
			if($enddate) {
				$enddate = date_i18n( 'l j F' , strtotime($enddate));
			}
			$thumb_url = get_the_post_thumbnail_url($post = null, $size = 'list-thumb');
			$terugblik = get_post_meta(get_the_ID(), 'terugblik', true);
			echo "<div class='row mb1 pb1'>";
					echo "<div class='col-12 col-sm-4'>";

						echo "<a href='". $permalink ."'>";
						if($thumb_url) {
							echo "<img src='" . $thumb_url . "' alt='Featured Image' class='responsive' />";
						}
						echo "</a>";
					echo "</div>";
					echo "<div class='col-12 col-sm-8'>";

						echo "<a href='". $permalink ."'>";
						if($enddate) {
							echo "<div class='date summary'>" . $m_meta_datum . " - " . $enddate . "</div>";

						}
						else {
							echo "<div class='date summary'>" . $m_meta_datum . "</div>";
						}
							the_title('<h5>', '</h5>');
							if ( has_excerpt() ) {
								echo "<p>" . $excerpt . "</p>";
							}
							if ( ! empty( $categories ) ) {
								echo "<div class='cat'>" . esc_html( $categories[0]->name ) . "</div>";   
							}
						echo "</a>";
					echo "</div>";// item
				echo "</a>";
			echo "</div>";
		}
	}
	// Restore original post data.
	wp_reset_postdata();
}


function past_events() {
	$args = array(
		'post_type' => 'event',
		'meta_query' => array(
			'terugblik' => array(
				'key' => 'terugblik',
				'value' => 1,
				'compare' => '=' 
			),
			'sortering' => array(
				'key' => 'meta-box-extra-date',
			),			
		),	
		'orderby' => 'sortering',
	);

	// Custom query.
	$query = new WP_Query( $args );

	// Check that we have query results.
	if ( $query->have_posts() ) {
		echo "<div class='row' id='frontrow'>";

		// Start looping over the query results.
		while ( $query->have_posts() ) {

			$query->the_post();
			$excerpt = get_the_excerpt();
			$permalink = get_permalink();
			$categories = get_the_category();
			$post_id = get_the_id();
			$m_meta_datum = get_post_meta(get_the_ID(), 'meta-box-extra-date', true);
			$m_meta_datum = date_i18n( 'l j F' , strtotime($m_meta_datum));
			$thumb_url = get_the_post_thumbnail_url($post = null, $size = 'list-thumb');
			$home_image = get_post_meta(get_the_ID(), 'meta-image', true);
			$terugblik = get_post_meta(get_the_ID(), 'terugblik', true);
			echo "<div class='col col-12 col-sm-6 col-lg-4'>";
			echo "<a href='". $permalink ."'>";
			if($home_image) {
				echo "<img src='" . $home_image . "' alt='Featured Image' class='responsive' />";
			}
			else if($thumb_url) {
				echo "<img src='" . $thumb_url . "' alt='Featured Image' class='responsive' />";
			}
			echo "<div class='date'>" . $m_meta_datum . "</div>";
			the_title('<h5>', '</h5>');
			if ( has_excerpt() ) {
				echo "<p>" . $excerpt . "</p>";
			}
			if ( ! empty( $categories ) ) {
				echo "<div class='cat'>" . esc_html( $categories[0]->name ) . "</div>";   
			}
			echo "</a>";
			echo "</div>";// item
			echo "</a>";
		}
		echo "<div class='col col-12 col-sm-6 col-lg-4 archief'>";
		echo "<a href='/archief'><h4>ARCHIEF</h4></a>";
		echo "<p>Bijeenkomst gemist?<br>Bekijk de verslagen in ons archief.";
		echo "</div>";

		echo "</div>";

	}
	// Restore original post data.
	wp_reset_postdata();
}
