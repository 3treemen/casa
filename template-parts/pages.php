<?php
/**
 * The default template for displaying content
 *
 * Used for both singular and index.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package WordPress
 * @subpackage Twenty_Twenty
 * @since 1.0.0
 */

?>

<article <?php post_class(); ?> id="post-<?php the_ID(); ?>">

	<?php
	// this shows the title and excerpt
	get_template_part( 'template-parts/entry-header' );

	
	?>
	<div class="post-inner">
		<div class="entry-content">

			<div class="container">
			<?php
				page_summary();
			?>
</div>


		</div><!-- .entry-content -->
	</div><!-- .post-inner -->




</article><!-- .post -->
