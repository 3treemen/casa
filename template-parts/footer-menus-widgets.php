<?php
/**
 * Displays the menus and widgets at the end of the main element.
 * Visually, this output is presented as part of the footer element.
 *
 * @package WordPress
 * @subpackage Twenty_Twenty
 * @since 1.0.0
 */

$has_sidebar_one = is_active_sidebar( 'sidebar-one' );
$has_sidebar_two = is_active_sidebar( 'sidebar-two' );
$has_sidebar_three = is_active_sidebar( 'sidebar-three' );
$has_sidebar_four = is_active_sidebar( 'sidebar-four' );

// Only output the container if there are elements to display.
if (  $has_sidebar_one || $has_sidebar_two || $has_sidebar_three || $has_sidebar_four ) {
	?>
			<?php if ( $has_sidebar_one || $has_sidebar_two || $has_sidebar_three || $has_sidebar_four ) { ?>
				<aside id="site-footer" class="site-footer section" role="complementary">
					<div class="container">	
						<div class="row">
								<div class="col-12 col-md-4">
									<?php if ( $has_sidebar_one ) { ?>
											<?php dynamic_sidebar( 'sidebar-one' ); ?>
									<?php } ?>
								</div>
								<div class="col-12 col-md-8">
									<div class="row">
										<div class="col-md-4 col-12">
											<?php if ( $has_sidebar_two ) { ?>
												<?php dynamic_sidebar( 'sidebar-two' ); ?>
											<?php } ?>
										</div>
										<div class="col-md-4 col-12">
											<?php if ( $has_sidebar_three ) { ?>
												<?php dynamic_sidebar( 'sidebar-three' ); ?>
											<?php } ?>
										</div>
										<div class="col-md-4 col-12">
											<?php if ( $has_sidebar_four ) { ?>
												<?php dynamic_sidebar( 'sidebar-four' ); ?>
											<?php } ?>
										</div>
									</div><!-- end inner row -->
								</div><!-- end col-8 -->
							</div><!-- end row -->
						</div><!-- end container -->
					</div>
				</aside><!-- .footer-widgets-outer-wrapper -->

			<?php } ?>


<?php } ?>
