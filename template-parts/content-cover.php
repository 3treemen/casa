<?php
/**
 * Displays the content when the cover template is used.
 *
 * @package WordPress
 * @subpackage Twenty_Twenty
 * @since 1.0.0
 */
?>
<?php get_template_part( 'template-parts/featured-image' ); ?>


<div class="post-inner" id="post-inner">
	<div class="entry-content">
		<div class="section-inner is-style-wide">
			<div class="row" id="frontrow">
				<?php
					// homepage_newsitems();
					newsitems_home();
					homepage_events(0); 
					homepage_events(1)
				?>
			</div>
		</div>
	</div><!-- .entry-content -->
		
	<section id="laposta">		
		<?php
			get_template_part( 'template-parts/laposta' );
		?>
	</section>

</div><!-- .post-inner -->

	
	
	
